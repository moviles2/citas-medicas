export class Medicos{
    uid:string
    cedula:string
    nombre:string
    apellido:string
    fechaNac:string
    telefono:string
    estadoCivil:string
    direccion:string
    especialidad:string
    email:string
    contrasena:string    
    foto:string
    descripcion:string
    rol:string;
    estado: string;

}