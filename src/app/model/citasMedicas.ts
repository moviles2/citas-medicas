import { StringDecoder } from "string_decoder";
import { Medicos } from "./medico";

export class CitasMedicas{
    uid:string
    paciente:string
    medico:string
    fecha:string
    hora:string
    sintomas:string
    estado:string
    estadoFactura:string
}