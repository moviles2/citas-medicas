import { StringDecoder } from "string_decoder";

export class RecetaMedica{
    uid:string
    nombre:string
    fechaCorta: string = new Date().toISOString();
    fecha: string = this.fechaCorta;
    //fecha:string
    peso:string
    talla:string
    rp:string
    diagnostico:string
    medicamento: string;
    medipaci:string;
}