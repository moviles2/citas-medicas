import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {


  message:Mensaje=new Mensaje();
  worked:boolean=false;
  constructor(public router: Router,
    public contactService:ContactosService,private route: ActivatedRoute) {
          this.route.queryParams.subscribe(params => {
          console.log(params);
          if (this.router.getCurrentNavigation().extras.queryParams) {
            this.message = this.router.getCurrentNavigation().extras.queryParams.contacto;
            console.log(this.message);
            }
        });
    
    
      }

     

  ngOnInit() {
  }

  guardar(){

    console.log(this.message)
    //navegar en diferente paginas
    this.contactService.saveContacto(this.message);
      let navigationExtras: NavigationExtras={
      queryParams:{
        message:this.message,
        worked:this.worked
      }
    };
   
  //dirigise a otra pagina y pasarle los parametros
  this.router.navigate(['/confirmacionmensaje'],navigationExtras);
   

  }
}
