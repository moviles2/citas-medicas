import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;

  correo='ciza'

  public appPages = [
    {
      title: 'Pagina Principal',
      url: '/pagina-principal',
      icon: 'home'
    },
    {
      title: 'Registrar Citas medicas',
      url: '/registrar-citas-medicas',
      icon: 'layers'
    },
    
    {
      title: 'Listar Citas medicas',
      url: '/listar-citas-medicas',
      icon: 'list'
    },
    {
      title: 'Registrar Receta Medica',
      url: '/registrar-receta-m',
      icon: 'heart'
    },
    {
      title: 'Listar Recetas Medicas',
      url: '/listar-contactos',
      icon: 'pulse'
    }
    ,
    {
      title: 'Api',
      url: '/api-name',
      icon: 'book'
    },
    {


      title: 'Maps',
      url: '/localizacion',
    },
    {

      title: 'Registrarse',
      url: '/registro',
      icon: 'book'
    },

  ];
 

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
