import { ɵPLATFORM_WORKER_UI_ID } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.page.html',
  styleUrls: ['./ver.page.scss'],
})
export class VerPage implements OnInit {

  medicoo: Observable<any[]>; 
  medicos: any[] = [];
  palabra = '';

  constructor(private medicoSrv: MedicoService, 
    private router: Router, private afs: AngularFirestore) { }

  ngOnInit() {
    this.medicoo = this.medicoSrv.getMedicos();
    this.medicoo.forEach(element => {
      console.log(element[0])
    });

    
  }
  
    buscar(event) {
      const palabra = event.target.value;
      this.palabra = palabra;
    }

  eliminarMedico(uid:string) {
    console.log('hola')
    this.medicoo.forEach(element => {
      console.log(element)
      console.log('uid de elemnete')
      console.log(uid)
      this.medicoSrv.updateMedicoAnular(uid)
    });
   
  }

  verMedico(medId: String){
    console.log("idddddddddddddddddddddddd" +  medId);
    this.router.navigate([`admin-editar-medico/${medId}`]);
  }

  editaMedicoById(id:string){
    const url = '/prueba/' + id;
    console.log(url);
    this.router.navigate([url]); 
  }

  
}
