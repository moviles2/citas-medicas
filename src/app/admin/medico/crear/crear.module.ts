import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearPageRoutingModule } from './crear-routing.module';

import { CrearPage } from './crear.page';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDMNQzWtX8FAgLoMFdrMs4KC55TnLoA6hk'
    })
  ],
  declarations: [CrearPage]
})
export class CrearPageModule {}
