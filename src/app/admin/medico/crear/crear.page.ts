import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Medicos} from 'src/app/model/medico';
import { LocationService } from 'src/app/services/location.service';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.page.html',
  styleUrls: ['./crear.page.scss'],
})
export class CrearPage implements OnInit {

  medico: Medicos = new Medicos();
  constructor(private locationService:LocationService, private medicoSrv:MedicoService, private router:Router) { }

  title = 'My first AGM project';
  lat = -2.8969536605642725;
  lng = -79.00415549603791

  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }

  ngOnInit() {
  }

  onSubmitTemplate(){
    this.medicoSrv.onRegister(this.medico, this.newLocation);
  }

  setNewLocation(event){
    if(event){
      this.newLocation.latitude = event.lat;
      this.newLocation.longitude = event.lng;
      this.locationService.getAddressOfLocation(this.newLocation);
    }
  }

  registrarDos(newLocation){
     this.medicoSrv.saveMedico(this.medico,this.newLocation);
     this.router.navigateByUrl('inicio-admin');
   }


/*
registrarMedico(newLocation){
    console.log(this.medicoSrv.(this.medico));
    //console.log(this.medicoSrv.save(this.medico,this.newLocation));
    this.router.navigateByUrl('inicio-admin');

  }


  registrarDos(newLocation){
   // this.medicoService.saveMedico(this.medico,this.newLocation);
    this.router.navigateByUrl('inicio-admin');
  }
*/
}
