import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  uid:string 
  medico: any = {
    id: '',
  
  };

  constructor(private route: ActivatedRoute, 
    private medsrv: MedicoService, 
    private router: Router ) { 

      this.uid = this.route.snapshot.paramMap.get('id');
      console.log("consultando ", this.uid);
  
      this.medsrv.getMedicoById2(this.uid).subscribe(data => {
          console.log(data)
          const aux:any = data
          this.medico = aux[0];
      });
    }

   

  async ngOnInit() {
  }
  verMedico(medId: String){
    this.router.navigate([`admin-editar-medico/${medId}`]);
  }
  
  editar(){
    this.medsrv.editarMedico(this.medico);
    this.router.navigateByUrl(`admin-ver-medico`);
  }
}
