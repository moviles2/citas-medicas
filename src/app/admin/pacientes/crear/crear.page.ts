import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/model/paciente';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.page.html',
  styleUrls: ['./crear.page.scss'],
})
export class CrearPage implements OnInit {
  paci: Paciente = new Paciente();

  title = 'My first AGM project';
  lat = -2.8969536605642725;
  lng = -79.00415549603791


  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }

  constructor(private pacSr: PacienteService) { }

  ngOnInit() {
  }

  onSubmitTemplate(){

    this.pacSr.onRegister(this.paci, this.newLocation,this.paci.uid);
    console.log(this.paci);
  }

}
