import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente';
import { MedicoService } from 'src/app/services/medico.service';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  uid:string 
  paciente: any = {
    id: '',
   
  };

  constructor(private route: ActivatedRoute, 
    private pacsrv: PacienteService, 
    private router: Router) { 

      this.uid = this.route.snapshot.paramMap.get('uid');
      console.log("consultando ", this.uid);
  
      this.pacsrv.getPacienteById(this.uid).subscribe(data => {
          console.log(data)
          const aux:any = data
          this.paciente = aux[0];
      });
     }

  ngOnInit() {
  }

  eliminarpaciente(pacien: Paciente) {
    this.pacsrv.deletePaciente(pacien);
    this.router.navigateByUrl(`admin-ver-paciente`);
  }


  verPaciente(paId: String){
    this.router.navigate([`admin-editar-paciente/${paId}`]);
  }
  
  editarPac(){
    this.pacsrv.editarPaciente(this.paciente);
    this.router.navigateByUrl(`admin-ver-paciente`);
  }

}
