import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/model/paciente';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.page.html',
  styleUrls: ['./ver.page.scss'],
})
export class VerPage implements OnInit {

  pacientee: Observable<any[]>; 
  pacientes: any[] = [];
  busqueda = '';

  constructor(private paciSr: PacienteService, 
    private router: Router) { }

  ngOnInit() {
    this.pacientee = this.paciSr.getPacientes();
    this.pacientee.forEach(element => {
      console.log(element[0])
      });
    }

  

  buscarPacientesAdmin(event) {
    const busqueda = event.target.value;
    this.busqueda = busqueda;
  }

  verPaciente(pacId: String){
    
    this.router.navigate([`admin-editar-paciente/${pacId}`]);
  }

  editaPacienteById(uid:string){
    const url = '/pacientess/' + uid;
    console.log(url);
    this.router.navigate([url]); 
  }

  eliminarPaciente(uid:string) {
    console.log('hola')
    this.pacientee.forEach(element => {
      console.log(element)
      console.log('uid de elemnete')
      console.log(uid)
      this.paciSr.updatePacienteAnular(uid)
    });
   
  }



}
