import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { Admin } from 'src/app/model/admin';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-inicio-admin',
  templateUrl: './inicio-admin.page.html',
  styleUrls: ['./inicio-admin.page.scss'],
})
export class InicioAdminPage implements OnInit {


  admin: any = {

    id: '',
    data: {} as Admin
  };

  constructor(private popoverCtrl: PopoverController, private router: Router, private auth: AuthService) { }

  async ngOnInit() {
    (await this.auth.getUser()).subscribe(dat => {
      console.log(dat.uid);
      console.log("aaaaaaaaaaaaaaaaaaaa" + this.auth.getAdmin(dat.uid))
      this.auth.getAdmin(dat.uid).subscribe((datos) =>{
        if(datos.payload.data() !=null){
          this.admin.id = datos.payload.id;
          this.admin.data = datos.payload.data();
        }else{

          this.admin.data = {} as Admin;
        }
      });
    });
  }

  cerrarSesionAdmin(){
    
    this.auth.cerraSesion();
    this.router.navigate([`login`]);
  }

}
