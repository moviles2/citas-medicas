import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-listar-citas-medicas',
  templateUrl: './listar-citas-medicas.page.html',
  styleUrls: ['./listar-citas-medicas.page.scss'],
})
export class ListarCitasMedicasPage implements OnInit {
  citasMed: Observable< any []>;
  constructor(public router:Router,public contactosService:ContactosService) { }

  ngOnInit() {
    this.citasMed=this.contactosService.getCitasMedicas();

  }
  
  
}
