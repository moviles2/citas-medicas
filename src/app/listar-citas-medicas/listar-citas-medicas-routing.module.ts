import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarCitasMedicasPage } from './listar-citas-medicas.page';

const routes: Routes = [
  {
    path: '',
    component: ListarCitasMedicasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListarCitasMedicasPageRoutingModule {}
