import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListarCitasMedicasPage } from './listar-citas-medicas.page';

describe('ListarCitasMedicasPage', () => {
  let component: ListarCitasMedicasPage;
  let fixture: ComponentFixture<ListarCitasMedicasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarCitasMedicasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListarCitasMedicasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
