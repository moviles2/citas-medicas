import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarCitasMedicasPageRoutingModule } from './listar-citas-medicas-routing.module';

import { ListarCitasMedicasPage } from './listar-citas-medicas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarCitasMedicasPageRoutingModule
  ],
  declarations: [ListarCitasMedicasPage]
})
export class ListarCitasMedicasPageModule {}
