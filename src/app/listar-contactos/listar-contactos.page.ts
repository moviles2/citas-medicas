import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-listar-contactos',
  templateUrl: './listar-contactos.page.html',
  styleUrls: ['./listar-contactos.page.scss'],
})
export class ListarContactosPage implements OnInit {

  recetaMed: Observable< any []>;
  constructor(public ContactosService:ContactosService,
     public router:Router) { 


  }

  ngOnInit() {
    this.recetaMed=this.ContactosService.getRecetaMedicas();
  }

  nuevoContacto(){
  this.router.navigate(['/contact']);

  }

editarContacto(contacto:Mensaje){

  let navigationExtras: NavigationExtras={
    queryParams:{
     contacto:contacto
    }
  };
  this.router.navigate(['/contact'],navigationExtras);
  
 }

 editarContactoById(uid:string){
   // para validad por un id

   this.router.navigate(['/contact/'+uid]);
 }
 



}
