import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApiNamePageRoutingModule } from './api-name-routing.module';

import { ApiNamePage } from './api-name.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApiNamePageRoutingModule
  ],
  declarations: [ApiNamePage]
})
export class ApiNamePageModule {}
