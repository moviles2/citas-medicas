import { Component, OnInit } from '@angular/core';
import {ProveedorApiService} from '../services/proveedor-api.service'

@Component({
  selector: 'app-api-name',
  templateUrl: './api-name.page.html',
  styleUrls: ['./api-name.page.scss'],
})
export class ApiNamePage implements OnInit {

  arrayPosts:any;

  constructor(public postServices:ProveedorApiService) { }

  ngOnInit() {
    this.postServices.obtenerDatos().subscribe(
      (data)=>{this.arrayPosts = data;},
      (error)=>{console.log(error);}
    )
  }

}
