import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApiNamePage } from './api-name.page';

const routes: Routes = [
  {
    path: '',
    component: ApiNamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApiNamePageRoutingModule {}
