import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApiNamePage } from './api-name.page';

describe('ApiNamePage', () => {
  let component: ApiNamePage;
  let fixture: ComponentFixture<ApiNamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiNamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApiNamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
