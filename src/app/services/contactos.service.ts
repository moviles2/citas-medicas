import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Mensaje } from '../model/mensaje';
import { first} from 'rxjs/operators';
import { CitasMedicas } from '../model/citasMedicas';
import { RecetaMedica } from '../model/recetaMedica';
@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  constructor(public afs:AngularFirestore) {}


    //Metod para guardar un documento
    //Como se va guardar la tabla en la base de datos
    saveContacto(contacto : Mensaje){//
      const refContacto=this.afs.collection("contactos");
      if(contacto.uid==null)
      contacto.uid=this.afs.createId();
      refContacto.doc(contacto.uid).set(Object.assign({},contacto),{merge:true})


    }

    getContactos(): Observable< any []>{
      return this.afs.collection("contactos").valueChanges();

    }

    async getContactoById(uid:string){
      try {
        let aux= await this.afs.collection("contactos",
          ref => ref.where('uid','==',uid))
                    .valueChanges().pipe(first()).toPromise().then(doc => {
                      return doc;

                    }).catch(error => {
                      throw error;
                      
                    });
          if(aux==null)
            return {};
          return aux[0];
      } catch (error) {
        console.error("Erros get contactos ById", error);
        throw error;
      }
    }
    getContactoById2(uid:string) : Observable <any>{
      return this.afs.collection("contactos",
      ref => ref.where('uid','==',uid))
                .valueChanges();
    }

    //citas medicas
    
saveCitasMedicas(citasMed : CitasMedicas){//
  const refCitaMedica=this.afs.collection("citaMedica");
  if(citasMed.uid==null)
  citasMed.uid=this.afs.createId();
  refCitaMedica.doc(citasMed.uid).set(Object.assign({},citasMed),{merge:true})


}


getCitasMedicas(): Observable< any []>{
  return this.afs.collection("citaMedica").valueChanges();

}

//receta medica

saveRecetaMedicas(recetasMed : RecetaMedica){//
  const refRecetaMedica=this.afs.collection("recetaMedica");
  if(recetasMed.uid==null)
  recetasMed.uid=this.afs.createId();
  refRecetaMedica.doc(recetasMed.uid).set(Object.assign({},recetasMed),{merge:true})


}


getRecetaMedicas(): Observable< any []>{
  return this.afs.collection("recetaMedica").valueChanges();

}

}


