import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { CitasMedicas } from '../model/citasMedicas';
import { Paciente } from '../model/paciente';

@Injectable({
  providedIn: 'root'
})
export class CitasMedicasService {
  angularFirestore: any;

  constructor(public afs:AngularFirestore) { 

  }

  saveCitaMedica(citaMed : CitasMedicas, paciente){//
    const refCitaMed=this.afs.collection("citaMedica");
    if(citaMed.uid==null)
    citaMed.uid=this.afs.createId();
    citaMed.paciente=paciente
    refCitaMed.doc(citaMed.uid).set(Object.assign({},citaMed),{merge:true})
  }

  getCitaMedica(): Observable< any []>{
  
    return this.afs.collection("citaMedica").valueChanges();

  }

  getCitaMedicaPendientes(paciente): Observable< any []>{
    console.log("servicio de obtencion de citas medicas",paciente)
    return this.afs.collection('citaMedica',
    ref => ref.where('paciente.data.nombres', '==', paciente ).where('estado', '==','pendiente')).valueChanges();
  }
  
  getCitaMedicaAprovadas(paciente): Observable< any []>{
    console.log("servicio de obtencion de citas medicas",paciente)
    return this.afs.collection('citaMedica',
    ref => ref.where('paciente.data.nombres', '==', paciente ).where('estado', '==','concluso')).valueChanges();
  }
  getCitaMedicaRechasadas(paciente): Observable< any []>{
    console.log("servicio de obtencion de citas medicas rechazadas",paciente)
    return this.afs.collection('RechazoCita',
    ref => ref.where('pasop.paciente.data.nombres', '==', paciente ).where('pasop.estado', '==','pendiente')).valueChanges();
  }

  getFacturasAprovadas(paciente): Observable< any []>{
    console.log("servicio de obtencion de citas medicas",paciente)
    return this.afs.collection('facturaPaciente',
    ref => ref.where('pasopm.paciente.data.nombres', '==', paciente )).valueChanges();
  }

  getCitaMedica3(paciente, medico): Observable< any []>{
    console.log("servicio de obtencion de citas medicas",paciente)
    return this.afs.collection('citaMedica',
    //paciente.data.nombres
    ref =>ref.where('paciente.data.nombres', '==', paciente).where('medico.nombre', '==',medico)).valueChanges();
  }

  
    // Obtener todos los pacientes2
    getPacientes2(paciente): Observable<any[]> {
      console.log("estoy en el servicio paciente 2")
      return this.afs.collection('usuarios',
      ref => ref.where('nombres', '==', paciente )).valueChanges();
    }
     // Obtener todos los medico
     getMedicos(medico): Observable<any[]> {
      console.log("estoy en el servicio medico")
    console.log(medico)
      return this.afs.collection('medicos',
        ref => ref.where('nombre', '==', medico )).valueChanges();
    }

    getReceta(paciente): Observable< any []>{
      console.log("servicio de obtencion de citas medicas",paciente)
      return this.afs.collection('recetaMedica',
      //paciente.data.nombres
      ref =>ref.where('medipaci.paciente.data.nombres', '==', paciente)).valueChanges();
    }

    editarMedico(paciente: Paciente){
        const refContacto = this.afs.collection("usuarios");
        if (paciente.uid == null) {
          paciente.uid = this.afs.createId();
          paciente.rol = 'paciente';
          paciente.estado = 'activo';
        }
    
        refContacto.doc(paciente.uid).set(Object.assign({}, paciente), { merge: true })
    
      
    }
    getPacienteById(uid: string): Observable<any> {
      return this.afs.collection("usuarios",
        ref => ref.where('uid', '==', uid))
        .valueChanges();
    }
  

}