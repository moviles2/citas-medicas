import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CitasMedicas } from '../model/citasMedicas';
import { Factura } from '../model/factura';
import { Medicos } from '../model/medico';
import { Paciente } from '../model/paciente';
import { RecetaMedica } from '../model/recetaMedica';
import { RechazoCita } from '../model/RechazoCita';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  citas: CitasMedicas = new CitasMedicas();
  citass: Observable<any[]>;


  [x: string]: any;
  forEach(arg0: (element: any) => void) {
    throw new Error('Method not implemented.');
  }

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private alertController: AlertController) {

   }
   

  getMedicos(): Observable<any[]> {
    return this.afs.collection("medicos",
      ref => ref.where("estado", "==", "activo")).valueChanges();
  }

  // RegistrarPaciente
  async onRegister(medico: Medicos, newLocation) {
    try {
      const pac = await this.afAuth.createUserWithEmailAndPassword(medico.email, medico.contrasena);
      medico.rol = 'medico';
      medico.direccion = newLocation
      const param = JSON.parse(JSON.stringify(medico));
      if (medico.uid == null) {

        medico.uid = this.afs.createId();

        this.afs.collection("medicos").doc(pac.user.uid).set(param);
      }
      this.presentAlert('Medico registrado exitosamente!');
    } catch (error) {
      this.presentAlert(error.message);
    }
  }

  getInicioSesionContacto(correo: string): Observable<any> {
    console.log(correo)
    return this.afs.collection("medicos",
      ref => ref.where('email', '==', correo).where('estado', '==', "activo"))
      .valueChanges();
  }


  // Mostrar alertas
  async presentAlert(mensaje: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      subHeader: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

  saveMedico(medico: Medicos, newLocation) {
    const refContacto = this.afs.collection("medicos");
    if (medico.uid == null) {
      medico.uid = this.afs.createId();
      medico.direccion = newLocation;
      medico.rol = 'medico';
      medico.estado = 'activo';
    }

    refContacto.doc(medico.uid).set(Object.assign({}, medico), { merge: true })

  }

  saveMedicoDos(medico: Medicos) {
    const refContacto = this.afs.collection("medicos");
    if (medico.uid == null) {
      medico.uid = this.afs.createId();
      medico.rol = 'medico';
      medico.estado = 'activo';
    }

    refContacto.doc(medico.uid).set(Object.assign({}, medico), { merge: true })

  }


  getCitasPendientes(): Observable<any[]> {
    return this.afs.collection("citaMedica",
      ref => ref.where("estado", "==", "pendiente")).valueChanges();

  }

  getCitasPendientesMostrar(correo: string): Observable<any> {
    console.log(correo)
    return this.afs.collection("citaMedica",
      ref => ref.where('medico.email', '==', correo).where("estado", "==", "pendiente"))
      .valueChanges();
  }

  getDoctorMostrar(correo: string): Observable<any> {
    //console.log(correo)
    return this.afs.collection("medicos", 
            ref => ref.where('email', '==', correo))
                      .valueChanges();
  }


  async getMedicoEdit(med: string) {
    return this.afs.collection<Medicos>('medicos', ref => ref.where('uid' , '==' , med)).snapshotChanges(); 
  }

async getMedicoById(email: string){
    try{
        let aux = await this.afs.collection("contactos", 
            ref => ref.where('email', '==', email))
                      .valueChanges().pipe(first()).toPromise().then(doc => {                    	  
                          return doc;
                      }).catch(error => {
                          throw error;
                      });
        if(aux==null)
            return {};
        return aux[0];
    }catch(error){
      console.error("Error get contactos ById", error);
      throw error;
    }
  }

  getMedicoUid(uid: string) {
    return this.angularFirestore.collection('medicos').doc(uid).snapshotChanges();
  }

  getMedicosUID(uid: string): Observable<any[]> {
    return this.afs.collection("medicos",
      ref => ref.where("uid", "==", uid)).valueChanges();

  }

  getMedicoById2(uid: string): Observable<any> {
    return this.afs.collection("medicos",
      ref => ref.where('uid', '==', uid))
      .valueChanges();
  }

  

  saveReceta(receta: RecetaMedica, paso: any) {
    const refContacto = this.afs.collection("recetaMedica");
    if (receta.uid == null) {
      receta.uid = this.afs.createId();
      receta.medipaci = paso;
    }

    refContacto.doc(receta.uid).set(Object.assign({}, receta), { merge: true })

  }

  updatePaciente(uid: string) {
    try {
      this.citass = this.afs.collection("citaMedica", ref => ref.where('uid', '==', uid)).valueChanges();
      this.afs.collection('citaMedica').doc('estado').set('concluso');
      this.presentAlert('Paciente actualizado correctamente');
    } catch (error) {
      this.presentAlert(error.message);
    }
  }

  getCitaById2(uid: string): Observable<any> {
    return this.afs.collection("citaMedica",
      ref => ref.where('uid', '==', uid))
      .valueChanges();
  }


  editarMedico(medico: Medicos){

    try{

      this.afs.collection('medicos').doc(medico.uid).set(medico);
      this.presentAlert('Datos Actualizados Correctamente');
    }catch(error){

      this.presentAlert(error.message)
    }
  }

  
  public updateCita(documentId: string) {
    return this.afs.collection("citaMedica").doc(documentId).set({estado:'concluso'}, { merge: true });

    }


  getCitasConcluidasMostrar(uid: string): Observable<any> {
    console.log(uid)
    return this.afs.collection("citaMedica",
      ref => ref.where('medico.uid', '==', uid).where("estado", "==", "concluso"))
      .valueChanges();
  }

  getCitasConcluidasFacturaMostrar(uid: string): Observable<any> {
    console.log(uid)
    return this.afs.collection("citaMedica",
      ref => ref.where('medico.uid', '==', uid).where("estado", "==", "concluso").where("estadoFactura", "==", "pendiente"))
      .valueChanges();
  }

  saveFactura(factura:Factura,paso:any) {
    const refContacto = this.afs.collection("facturaPaciente");
    if (factura.uid == null) {
      factura.uid = this.afs.createId();
      factura.pasopm = paso;
    }

    refContacto.doc(factura.uid).set(Object.assign({}, factura), { merge: true })
    
  }

  public updateFactura(documentId: string) {
    return this.afs.collection("citaMedica").doc(documentId).set({ estadoFactura: 'concluido' }, { merge: true });
  }

  saveRechazo(rechazo:RechazoCita,paso:any) {
    const refContacto = this.afs.collection("RechazoCita");
    if (rechazo.uid == null) {
      rechazo.uid = this.afs.createId();
      rechazo.pasop = paso;
    }

    refContacto.doc(rechazo.uid).set(Object.assign({}, rechazo), { merge: true })
    
  }

  public updateCitaRechazoEstaFact(documentId: string) {
    return this.afs.collection("citaMedica").doc(documentId).set({ estadoFactura: 'rechazado' }, { merge: true });
  }
  
  public updateCitaRechazoEsta(documentId: string) {
    return this.afs.collection("citaMedica").doc(documentId).set({ estado: 'rechazado' }, { merge: true });
  }

  public updateMedicoAnular(documentId: string) {
    return this.afs.collection("medicos").doc(documentId).set({ estado: 'inactivo'}, { merge: true });
  }

}

