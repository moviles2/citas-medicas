import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Paciente } from '../model/paciente';
import { Observable } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { CitasMedicas } from '../model/citasMedicas';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  constructor(private afAuth: AngularFireAuth,
     private angularFirestore: AngularFirestore,
      private alertController: AlertController) { }

  // Obtener paciente
  getPaciente(uid: string) {
    return this.angularFirestore.collection('usuarios').doc(uid).snapshotChanges();
  }

  getPacienteById(uid: string): Observable<any> {
    return this.angularFirestore.collection("usuarios",
      ref => ref.where('uid', '==', uid))
      .valueChanges();
  }


  // Obtener todos los pacientes
  getPacientes(): Observable<any[]> {
    return this.angularFirestore.collection('usuarios',
      ref => ref.where('rol', '==', 'paciente').where('estado', '==', 'activo')).valueChanges();
  }
  // Obtener todos los pacientes2
  getPacientes2(): Observable<any[]> {
    return this.angularFirestore.collection('usuarios',
      ref => ref.where('rol', '==', 'paciente')).valueChanges();
  }

  getPacientesAdmin(): Observable<any[]> {
    return this.angularFirestore.collection('usuarios',
      ref => ref.where('rol', '==', 'paciente').where('estado', '==', 'activo')).valueChanges();
  }

  editarPaciente(paciente: Paciente){

    try{
      this.angularFirestore.collection('usuarios').doc(paciente.uid).set(paciente);
      this.presentAlert('Datos Actualizados Correctamente');
    }catch(error){

      this.presentAlert(error.message)
    }
  }
  
  // RegistrarPaciente
  async onRegister(paciente: Paciente, newLocation, uid : string) {
    try {
      
      const pac = await this.afAuth.createUserWithEmailAndPassword(paciente.email, paciente.contrasena);
      console.log(pac)
      paciente.rol = 'paciente';
      paciente.estado = 'activo';
      paciente.direccion = newLocation
      if (paciente.uid == null) {
        console.log("entra al if")
        paciente.uid =  this.angularFirestore.createId(); // pac.user.uid;
      }
      const param = JSON.parse(JSON.stringify(paciente));
      console.log("parametros que llegan" +  param)
      this.angularFirestore.collection("usuarios").doc(pac.user.uid).set(param);
      this.presentAlert('Paciente registrado exitosamente!');
    } catch (error) {
      this.presentAlert(error.message);
    }
  }

  // Eliminar paciente
  deletePaciente(paciente: Paciente) {
    try {
      this.angularFirestore.collection('usuarios').doc(paciente.uid).delete();
      this.presentAlert('Paciente eliminado');
    } catch (error) {
      this.presentAlert(error.message);
    }
  }

  public updatePacienteAnular(documentId: string) {
    return this.angularFirestore.collection("usuarios").doc(documentId).set({ estado: 'inactivo'}, { merge: true });
     this.presentAlert('Paciente eliminado');
  }

  
  async presentAlert(mensaje: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      subHeader: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }


}
