import { Component, OnInit } from '@angular/core';
import { Paciente } from '../model/paciente';
import { PacienteService } from '../services/paciente.service';
import { LocationService } from './location.service';

@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.page.html',
  styleUrls: ['./crear-paciente.page.scss'],
})
export class CrearPacientePage implements OnInit {
  paciente: Paciente = new Paciente();
  title = 'My first AGM project';
  lat = -2.8969536605642725;
  lng = -79.00415549603791

  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }

  constructor(private pacienteSrv: PacienteService,private locationService:LocationService) { }

  ngOnInit() {
  }

  onSubmitTemplate(){
    this.pacienteSrv.onRegister(this.paciente, this.newLocation, this.paciente.uid);
  }

}
