import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // tslint:disable-next-line: max-line-length
  constructor(public afAuth: AngularFireAuth, private angularFirestore: AngularFirestore, private alertController: AlertController, private router: Router) { }

  // Obtener usuario de la sesion
  async getUser() {
    return this.afAuth.authState;
  }

  // Obtener informacion del usuario en ela sesion
  getDataUser(uid: string) {
    return this.angularFirestore.collection('usuarios').doc(uid).snapshotChanges();
  }

  // Actualizar informacion de usuario
  updateUser(usuario) {
    try {
      this.angularFirestore.collection('usuarios').doc(usuario.data.uid).set(usuario.data);
      if (usuario.img !== ''){
        this.angularFirestore.collection('usuarios').doc(usuario.data.uid).update({img: usuario.img});
      }
      this.presentAlert('Información actualizada correctamente', 'Listo');
    } catch (error) {
      this.presentAlert(error.message, 'Error');
    }
  }

  // Actualizar contrasena
  async updatePassword(password: string) {
    try{
      (await this.afAuth.currentUser).updatePassword(password);
      (await this.getUser()).subscribe(user => {
        this.angularFirestore.collection('usuarios').doc(user.uid).update({contrasena: password});
      });
      await this.presentAlert('Contraseña actualizada', 'Listo');
    } catch (error) {
      await this.presentAlert(error.message, 'Error');
    }
  }

  // IniciarSesion
  
  async onLogin(user: User) {
    try{
      const us = await this.afAuth.signInWithEmailAndPassword(user.email, user.contrasena);
      this.angularFirestore.doc<any>(`usuarios/${us.user.uid}`).valueChanges().subscribe( usuario => {
        console.log(usuario);
        if (usuario.rol === 'paciente'){
          let navigationExtras: NavigationExtras={
            queryParams:{
              usuario:usuario
            }
          };
  //        this.router.navigateByUrl('/pag-principal-usuario',navigationExtras);

          this.router.navigateByUrl('pag-principal-usuario');
        } else if (usuario.rol === 'medico'){
          this.router.navigateByUrl('pagina-inicial');
          //console.log('eres medico');
        } else if (usuario.rol === 'admin'){
          this.router.navigateByUrl('inicio-admin');
        }
      });
    } catch (error) {
      this.presentAlert(error.message, 'Error al iniciar sesión');
    }
  }

  cerraSesion(){
    this.afAuth.signOut;
  }


  // Mostrar alertas
  async presentAlert(mensaje: string, titulo: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titulo,
      subHeader: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

  getAdmin(uid: string){
    return this.angularFirestore.collection('usuarios').doc(uid).snapshotChanges();
  }
}
