import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PacientessPage } from './pacientess.page';

describe('PacientessPage', () => {
  let component: PacientessPage;
  let fixture: ComponentFixture<PacientessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacientessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PacientessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
