import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacientessPage } from './pacientess.page';

const routes: Routes = [
  {
    path: '',
    component: PacientessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientessPageRoutingModule {}
