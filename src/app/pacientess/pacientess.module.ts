import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PacientessPageRoutingModule } from './pacientess-routing.module';

import { PacientessPage } from './pacientess.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PacientessPageRoutingModule
  ],
  declarations: [PacientessPage]
})
export class PacientessPageModule {}
