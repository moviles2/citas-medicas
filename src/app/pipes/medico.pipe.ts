import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'medico'
})
export class MedicoPipe implements PipeTransform {

  transform(medico: any[], conver: string): any[] {
    if (conver.length === 0){
      return medico;
      
    }
    conver = conver.toLocaleLowerCase();

    return medico.filter( medi => {
      return medi.nombre.toLocaleLowerCase().includes(conver) || medi.apellido.toLocaleLowerCase().includes(conver);
    });
  }
}
