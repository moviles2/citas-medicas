import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paciente'
})
export class PacientePipe implements PipeTransform {

  transform(pacient: any[], convert: string): any[] {
    if(convert.length === 0) {
      return pacient;
    }
    convert = convert.toLocaleLowerCase();

    return pacient.filter( paci => {
      return paci.nombre.toLocaleLowerCase().includes(convert) || 
       paci.apellido.toLocaleLowerCase().includes(convert);
    });
  }
}
