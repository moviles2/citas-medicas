import { NgModule } from '@angular/core';
import { MedicoPipe } from './medico.pipe';
import { PacientePipe } from './paciente.pipe';

@NgModule({
  declarations: [MedicoPipe, PacientePipe],
  exports: [MedicoPipe, PacientePipe]
})
export class PipesModule { }
