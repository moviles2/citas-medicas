import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then(m => m.FolderPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then(m => m.RegistroPageModule)
  },
  {
    path: 'contact/:uid',
    loadChildren: () => import('./contact2/contact2.module').then(m => m.Contact2PageModule)
  },
  {
    path: 'confirmacionmensaje',
    loadChildren: () => import('./confirmacionmensaje/confirmacionmensaje.module').then(m => m.ConfirmacionmensajePageModule)
  },
  {
    path: 'confirmacionmensaje',
    loadChildren: () => import('./confirmacionmensaje/confirmacionmensaje.module').then(m => m.ConfirmacionmensajePageModule)
  },
  {
    path: 'listar-contactos',
    loadChildren: () => import('./listar-contactos/listar-contactos.module').then(m => m.ListarContactosPageModule)
  },
  {
    path: 'listar-citas-medicas',
    loadChildren: () => import('./listar-citas-medicas/listar-citas-medicas.module').then(m => m.ListarCitasMedicasPageModule)
  },
  {
    path: 'registrar-citas-medicas',
    loadChildren: () => import('./registrar-citas-medicas/registrar-citas-medicas.module').then(m => m.RegistrarCitasMedicasPageModule)
  },
  {
    path: 'registrar-receta-m',
    loadChildren: () => import('./registrar-receta-m/registrar-receta-m.module').then(m => m.RegistrarRecetaMPageModule)
  },
  {
    path: 'pagina-principal',
    loadChildren: () => import('./pagina-principal/pagina-principal.module').then(m => m.PaginaPrincipalPageModule)
  },
  {
    path: 'api-name',
    loadChildren: () => import('./api-name/api-name.module').then(m => m.ApiNamePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },

  {
    path: 'pag-principal-usuario',
    loadChildren: () => import('./usuario/pag-principal-usuario/pag-principal-usuario.module').then(m => m.PagPrincipalUsuarioPageModule)
  },
  {
    path: 'detalle-medicos',
    loadChildren: () => import('./usuario/detalle-medicos/detalle-medicos.module').then(m => m.DetalleMedicosPageModule)
  },
  {
    path: 'recervar-cita-medica',
    loadChildren: () => import('./usuario/recervar-cita-medica/recervar-cita-medica.module').then(m => m.RecervarCitaMedicaPageModule)
  },
  {
    path: 'ver-receta',
    loadChildren: () => import('./usuario/ver-receta/ver-receta.module').then(m => m.VerRecetaPageModule)
  }
  ,
  {
    path: 'inicio-admin',
    loadChildren: () => import('./admin/inicio-admin/inicio-admin.module').then(m => m.InicioAdminPageModule)
  },
  {
    path: 'admin-crear-medico',
    loadChildren: () => import('./admin/medico/crear/crear.module').then(m => m.CrearPageModule)
  },
  {
    path: 'admin-editar-medico',
    loadChildren: () => import('./admin/medico/editar/editar.module').then(m => m.EditarPageModule)
  },
  {
    path: 'admin-ver-medico',
    loadChildren: () => import('./admin/medico/ver/ver.module').then(m => m.VerPageModule)
  },
  {
    path: 'admin-ver-medico/:uid',
    loadChildren: () => import('./medicos/cuentaDos/crear-dos/crear-dos.module').then(m => m.CrearDosPageModule)
  },
  {
    path: 'admin-crear-paciente',
    loadChildren: () => import('./admin/pacientes/crear/crear.module').then(m => m.CrearPageModule)
  },
  {
    path: 'admin-editar-paciente',
    loadChildren: () => import('./admin/pacientes/editar/editar.module').then(m => m.EditarPageModule)
  },
  {
    path: 'admin-ver-paciente',
    loadChildren: () => import('./admin/pacientes/ver/ver.module').then(m => m.VerPageModule)
  },
  {
    path: 'pagina-inicial',
    loadChildren: () => import('./medicos/pagina-inicial/pagina-inicial.module').then(m => m.PaginaInicialPageModule)
  },
  {
    path: 'cuenta-medicos',
    loadChildren: () => import('./medicos/Cuenta/cuenta-medicos/cuenta-medicos.module').then(m => m.CuentaMedicosPageModule)
  },
  {
    path: 'citas-medicas-asignadas',
    loadChildren: () => import('./medicos/Citas/citas-medicas-asignadas/citas-medicas-asignadas.module').then(m => m.CitasMedicasAsignadasPageModule)
  },
  {
    path: 'iniciarsesion-medicos',
    loadChildren: () => import('./medicos/inicio/iniciarsesion-medicos/iniciarsesion-medicos.module').then(m => m.IniciarsesionMedicosPageModule)
  },
  {
    path: 'recetar-al-paciente',
    loadChildren: () => import('./medicos/receta/recetar-al-paciente/recetar-al-paciente.module').then(m => m.RecetarAlPacientePageModule)
  },
  {
    path: 'informacion-personal',
    loadChildren: () => import('./usuario/informacion-personal/informacion-personal.module').then(m => m.InformacionPersonalPageModule)
  },

  {
    path: 'admin-crear-medico',
    loadChildren: () => import('./admin/medico/crear/crear.module').then(m => m.CrearPageModule)
  },
  {
    path: 'admin-editar-medico/:uid',
    loadChildren: () => import('./admin/medico/editar/editar.module').then(m => m.EditarPageModule)
  },
  {
    path: 'admin-ver-med',
    loadChildren: () => import('./admin/medico/ver/ver.module').then(m => m.VerPageModule)
  },
  {
    path: 'admin-ver-medico/:uid',
    loadChildren: () => import('./medicos/cuentaDos/crear-dos/crear-dos.module').then(m => m.CrearDosPageModule)
  },
  {
    path: 'admin-crear-paciente',
    loadChildren: () => import('./admin/pacientes/crear/crear.module').then(m => m.CrearPageModule)
  },
  {
    path: 'admin-editar-paciente',
    loadChildren: () => import('./admin/pacientes/editar/editar.module').then(m => m.EditarPageModule)
  },
  {
    path: 'admin-ver-paciente',
    loadChildren: () => import('./admin/pacientes/ver/ver.module').then(m => m.VerPageModule)
  },
  {
    path: 'pagina-inicial',
    loadChildren: () => import('./medicos/pagina-inicial/pagina-inicial.module').then(m => m.PaginaInicialPageModule)
  },
  {
    path: 'cuenta-medicos',
    loadChildren: () => import('./medicos/Cuenta/cuenta-medicos/cuenta-medicos.module').then(m => m.CuentaMedicosPageModule)
  },
  {
    path: 'citas-medicas-asignadas',
    loadChildren: () => import('./medicos/Citas/citas-medicas-asignadas/citas-medicas-asignadas.module').then(m => m.CitasMedicasAsignadasPageModule)
  },
  {
    path: 'iniciarsesion-medicos',
    loadChildren: () => import('./medicos/inicio/iniciarsesion-medicos/iniciarsesion-medicos.module').then(m => m.IniciarsesionMedicosPageModule)
  },
  {
    path: 'recetar-al-paciente',
    loadChildren: () => import('./medicos/receta/recetar-al-paciente/recetar-al-paciente.module').then(m => m.RecetarAlPacientePageModule)
  },

  {
    path: 'factura',
    loadChildren: () => import('./medicos/facturaPaciente/factura/factura.module').then(m => m.FacturaPageModule)
  },
  {
    path: 'fact-datos-paciente',
    loadChildren: () => import('./medicos/facturarDatos/fact-datos-paciente/fact-datos-paciente.module').then(m => m.FactDatosPacientePageModule)
  },
  {
    path: 'rechazar-cita',
    loadChildren: () => import('./medicos/RechazarPaciente/rechazar-cita/rechazar-cita.module').then(m => m.RechazarCitaPageModule)
  },
  {
    path: 'factura-paciente',

    loadChildren: () => import('./usuario/factura-paciente/factura-paciente.module').then(m => m.FacturaPacientePageModule)
  },
  {
    path: 'prueba',
    loadChildren: () => import('./prueba/prueba.module').then( m => m.PruebaPageModule)
  },
  {
    path: 'prueba/:id',
    loadChildren: () => import('./admin/medico/editar/editar.module').then(m => m.EditarPageModule)
  },
  {
    path: 'pacientess',
    loadChildren: () => import('./pacientess/pacientess.module').then( m => m.PacientessPageModule)
  },
  {
    path: 'pacientess/:uid',
    loadChildren: () => import('./admin/pacientes/editar/editar.module').then(m => m.EditarPageModule)
  },
  { path: 'politica-privacidad',
  loadChildren: () => import('./politica/politica-privacidad/politica-privacidad.module').then( m => m.PoliticaPrivacidadPageModule)
},  {
    path: 'anonimo-listar-espe',
    loadChildren: () => import('./anonimo-listar-espe/anonimo-listar-espe.module').then( m => m.AnonimoListarEspePageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}



  







  








