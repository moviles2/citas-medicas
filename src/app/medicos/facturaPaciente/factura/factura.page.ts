import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CitasMedicas } from 'src/app/model/citasMedicas';
import { Factura } from 'src/app/model/factura';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.page.html',
  styleUrls: ['./factura.page.scss'],
})
export class FacturaPage implements OnInit {

  //medico: CitasMedicas
  pasood:any
  citas: Observable<any[]>;
  factura: Factura = new Factura();
  //cita: CitasMedicas = new CitasMedicas();
  
  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) { 
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.pasood = this.router.getCurrentNavigation().extras.state.msscitas;
      }
    });
  }

  ngOnInit() {
    this.citas = this.mediserv.getCitasConcluidasFacturaMostrar(this.pasood);
    console.log('lllllll')
    this.citas.forEach(element => {
      console.log(element[0])
    });
  }


  /*
  facturar(){
    console.log('-------------')
    //console.log(medico)
    //this.mediserv.saveFactura(this.factura,citas)
    this.citas.forEach(element => {
      console.log(element[0])
      this.mediserv.saveFactura(this.factura,element[0])
      console.log(this.factura.uid)
      this.mediserv.updateFactura(element[0].uid);
      console.log(element[0].uid)
     
    });

    this.router.navigateByUrl('pagina-inicial');
  }
  */
 facturar(citas){
  let params: NavigationExtras = {
    state:{
      msggcitas:citas,
    }
  }

  console.log('hkjhsdffdghdfsjgdhsfhsdg')
  console.log(params)
  this.router.navigate(['/fact-datos-paciente'],params);

 }

}
