import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FactDatosPacientePage } from './fact-datos-paciente.page';

describe('FactDatosPacientePage', () => {
  let component: FactDatosPacientePage;
  let fixture: ComponentFixture<FactDatosPacientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactDatosPacientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FactDatosPacientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
