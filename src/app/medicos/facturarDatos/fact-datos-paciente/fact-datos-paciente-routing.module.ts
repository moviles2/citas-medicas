import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactDatosPacientePage } from './fact-datos-paciente.page';

const routes: Routes = [
  {
    path: '',
    component: FactDatosPacientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FactDatosPacientePageRoutingModule {}
