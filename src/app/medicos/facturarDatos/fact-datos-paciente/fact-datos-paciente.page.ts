import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Factura } from 'src/app/model/factura';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-fact-datos-paciente',
  templateUrl: './fact-datos-paciente.page.html',
  styleUrls: ['./fact-datos-paciente.page.scss'],
})
export class FactDatosPacientePage implements OnInit {

  paso:any
  factura: Factura = new Factura();

  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) {
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.paso = this.router.getCurrentNavigation().extras.state.msggcitas;
        
      }
    });
   }

  ngOnInit() {
    console.log('paso de parametro')
    console.log(this.paso);
    console.log(this.paso.uid)
  }

  Voyafacturar(){
    this.mediserv.saveFactura(this.factura,this.paso);
    this.mediserv.updateFactura(this.paso.uid);
    this.router.navigateByUrl('pagina-inicial');
  }

}
