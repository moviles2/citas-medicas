import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FactDatosPacientePageRoutingModule } from './fact-datos-paciente-routing.module';

import { FactDatosPacientePage } from './fact-datos-paciente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FactDatosPacientePageRoutingModule
  ],
  declarations: [FactDatosPacientePage]
})
export class FactDatosPacientePageModule {}
