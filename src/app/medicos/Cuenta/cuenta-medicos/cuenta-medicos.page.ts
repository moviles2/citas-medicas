import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-cuenta-medicos',
  templateUrl: './cuenta-medicos.page.html',
  styleUrls: ['./cuenta-medicos.page.scss'],
})
export class CuentaMedicosPage implements OnInit {
  medicos: Observable<any[]>;
  medico: Medicos = new Medicos();
  uid:string;

  constructor(public mediserv:MedicoService,private route:ActivatedRoute,private router:Router) { 
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.uid = this.router.getCurrentNavigation().extras.state.msg;
        console.log(this.uid);
      }
    });
  }

  ngOnInit() {
      this.medicos = this.mediserv.getMedicosUID(this.uid);
  }

  editarContacto(contacto:Medicos){
    let params: NavigationExtras = {
      state:{
        medi:contacto, 
      }
    };

    this.router.navigate(['/admin-ver-medico'],params);

  }

  editaContacto2ById(uid:string){
    const url = '/admin-ver-medico/' + uid;
    console.log(url);
    this.router.navigate([url]); 
  }
  

  
}
