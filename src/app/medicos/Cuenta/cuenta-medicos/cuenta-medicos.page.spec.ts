import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CuentaMedicosPage } from './cuenta-medicos.page';

describe('CuentaMedicosPage', () => {
  let component: CuentaMedicosPage;
  let fixture: ComponentFixture<CuentaMedicosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaMedicosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CuentaMedicosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
