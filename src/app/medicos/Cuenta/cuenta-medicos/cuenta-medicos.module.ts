import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentaMedicosPageRoutingModule } from './cuenta-medicos-routing.module';

import { CuentaMedicosPage } from './cuenta-medicos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuentaMedicosPageRoutingModule
  ],
  declarations: [CuentaMedicosPage]
})
export class CuentaMedicosPageModule {}
