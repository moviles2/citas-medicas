import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentaMedicosPage } from './cuenta-medicos.page';

const routes: Routes = [
  {
    path: '',
    component: CuentaMedicosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentaMedicosPageRoutingModule {}
