import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Medicos } from 'src/app/model/medico';
import { LocationService } from 'src/app/services/location.service';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-iniciarsesion-medicos',
  templateUrl: './iniciarsesion-medicos.page.html',
  styleUrls: ['./iniciarsesion-medicos.page.scss'],
})
export class IniciarsesionMedicosPage implements OnInit {
  title = 'My first AGM project';
  lat = -2.8969536605642725;
  lng = -79.00415549603791


  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }

  

  medico: Medicos = new Medicos();
  email:string 
  contrasena:string

  constructor(private locationService:LocationService,public router:Router,public medicoService:MedicoService,private route:ActivatedRoute) {
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        //del lista-contacto se esta mandando un parametro contacto
        this.email = this.router.getCurrentNavigation().extras.state.contacto;
      }
    });
   }

  ngOnInit() {
  }


  setNewLocation(event){
    if(event){
      this.newLocation.latitude = event.lat;
      this.newLocation.longitude = event.lng;
      this.locationService.getAddressOfLocation(this.newLocation);
    }
  }
  
  login(){
    this.medicoService.getInicioSesionContacto(this.email).subscribe(data => {
      console.log(data)
      const aux:any = data
      this.medico = aux[0];
      const param = JSON.parse(JSON.stringify(data));
      console.log('objeyo')
      console.log(param[0])
      if(this.email==this.medico.email && this.contrasena==this.medico.contrasena)
      {
        let params: NavigationExtras = {
          state:{
            msg:this.email,
            medico:param[0],
          }
        }
        console.log(params)
        this.router.navigate(['/pagina-inicial'],params);
     
      }else{
        this.router.navigate(['/iniciarsesion-medicos']);
      }
    
  });
  }

  regresar(){
    this.router.navigate(['/login']);
  }

}
