import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IniciarsesionMedicosPage } from './iniciarsesion-medicos.page';

const routes: Routes = [
  {
    path: '',
    component: IniciarsesionMedicosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IniciarsesionMedicosPageRoutingModule {}
