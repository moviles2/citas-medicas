import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IniciarsesionMedicosPageRoutingModule } from './iniciarsesion-medicos-routing.module';

import { IniciarsesionMedicosPage } from './iniciarsesion-medicos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IniciarsesionMedicosPageRoutingModule
  ],
  declarations: [IniciarsesionMedicosPage]
})
export class IniciarsesionMedicosPageModule {}
