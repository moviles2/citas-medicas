import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RechazarCitaPageRoutingModule } from './rechazar-cita-routing.module';

import { RechazarCitaPage } from './rechazar-cita.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RechazarCitaPageRoutingModule
  ],
  declarations: [RechazarCitaPage]
})
export class RechazarCitaPageModule {}
