import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RechazarCitaPage } from './rechazar-cita.page';

describe('RechazarCitaPage', () => {
  let component: RechazarCitaPage;
  let fixture: ComponentFixture<RechazarCitaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechazarCitaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RechazarCitaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
