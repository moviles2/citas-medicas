import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RechazarCitaPage } from './rechazar-cita.page';

const routes: Routes = [
  {
    path: '',
    component: RechazarCitaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechazarCitaPageRoutingModule {}
