import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RechazoCita } from 'src/app/model/RechazoCita';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-rechazar-cita',
  templateUrl: './rechazar-cita.page.html',
  styleUrls: ['./rechazar-cita.page.scss'],
})
export class RechazarCitaPage implements OnInit {

  paso:any
  rechazo: RechazoCita = new RechazoCita();

  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) {
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.paso = this.router.getCurrentNavigation().extras.state.msggci;
        
      }
    });
   }

  ngOnInit() {
    console.log('cita-recha')
    console.log(this.paso)
  }


  RechazarCita(){
    this.mediserv.saveRechazo(this.rechazo,this.paso);
    this.mediserv.updateCitaRechazoEstaFact(this.paso.uid);
    this.mediserv.updateCitaRechazoEsta(this.paso.uid);
    this.router.navigateByUrl('pagina-inicial');
  }




}
