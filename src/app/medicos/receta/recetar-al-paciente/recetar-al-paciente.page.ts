import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CitasMedicas } from 'src/app/model/citasMedicas';
import { Factura } from 'src/app/model/factura';
import { RecetaMedica } from 'src/app/model/recetaMedica';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-recetar-al-paciente',
  templateUrl: './recetar-al-paciente.page.html',
  styleUrls: ['./recetar-al-paciente.page.scss'],
})
export class RecetarAlPacientePage implements OnInit {
  paso:any;
  receta:RecetaMedica = new RecetaMedica();
  factura:Factura = new Factura();
  citaas:CitasMedicas = new CitasMedicas();
  citas: Observable<any[]>;
  uid:string;

  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) {
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.paso = this.router.getCurrentNavigation().extras.state.msgg;
        
      }
    });
   }


  ngOnInit() {
    console.log('paso de parametro')
    console.log(this.paso);
    console.log('uid cita');
    console.log(this.paso.uid);
  }

  guardarReceta(paso){
   this.mediserv.saveReceta(this.receta,paso);
   let uid = paso.uid;
   console.log(this.mediserv.getCitaById2(uid));
   this.mediserv.updateCita(uid);
   this.router.navigateByUrl('pagina-inicial');
  }
}



