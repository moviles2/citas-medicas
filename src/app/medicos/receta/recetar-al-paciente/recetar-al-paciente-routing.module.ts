import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecetarAlPacientePage } from './recetar-al-paciente.page';

const routes: Routes = [
  {
    path: '',
    component: RecetarAlPacientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecetarAlPacientePageRoutingModule {}
