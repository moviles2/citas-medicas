import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecetarAlPacientePageRoutingModule } from './recetar-al-paciente-routing.module';

import { RecetarAlPacientePage } from './recetar-al-paciente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecetarAlPacientePageRoutingModule
  ],
  declarations: [RecetarAlPacientePage]
})
export class RecetarAlPacientePageModule {}
