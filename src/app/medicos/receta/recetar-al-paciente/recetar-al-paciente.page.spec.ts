import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecetarAlPacientePage } from './recetar-al-paciente.page';

describe('RecetarAlPacientePage', () => {
  let component: RecetarAlPacientePage;
  let fixture: ComponentFixture<RecetarAlPacientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecetarAlPacientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecetarAlPacientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
