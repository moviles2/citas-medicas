import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CitasMedicasAsignadasPageRoutingModule } from './citas-medicas-asignadas-routing.module';

import { CitasMedicasAsignadasPage } from './citas-medicas-asignadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CitasMedicasAsignadasPageRoutingModule
  ],
  declarations: [CitasMedicasAsignadasPage]
})
export class CitasMedicasAsignadasPageModule {}
