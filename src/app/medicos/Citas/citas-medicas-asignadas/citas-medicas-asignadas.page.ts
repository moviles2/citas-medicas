import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-citas-medicas-asignadas',
  templateUrl: './citas-medicas-asignadas.page.html',
  styleUrls: ['./citas-medicas-asignadas.page.scss'],
})
export class CitasMedicasAsignadasPage implements OnInit {

  pasoo:any;
  citas: Observable<any[]>;

  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) { 
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.pasoo = this.router.getCurrentNavigation().extras.state.mscitas;
        
      }
    });
  }

  ngOnInit() {
    console.log('pasoo-cuenta-asignadas')
    console.log(this.pasoo)
    this.citas = this.mediserv.getCitasConcluidasMostrar(this.pasoo);
  }


  


}
