import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CitasMedicasAsignadasPage } from './citas-medicas-asignadas.page';

const routes: Routes = [
  {
    path: '',
    component: CitasMedicasAsignadasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CitasMedicasAsignadasPageRoutingModule {}
