import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CitasMedicasAsignadasPage } from './citas-medicas-asignadas.page';

describe('CitasMedicasAsignadasPage', () => {
  let component: CitasMedicasAsignadasPage;
  let fixture: ComponentFixture<CitasMedicasAsignadasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitasMedicasAsignadasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CitasMedicasAsignadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
