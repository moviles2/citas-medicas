import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-pagina-inicial',
  templateUrl: './pagina-inicial.page.html',
  styleUrls: ['./pagina-inicial.page.scss'],
})
export class PaginaInicialPage implements OnInit {

  citas: Observable<any[]>;
  medicoN: Observable<any[]>;
  email:string;
  medico: Medicos = new Medicos();

  constructor(public mediserv:MedicoService,private route:ActivatedRoute,private router:Router) { 
    this.route.queryParams.subscribe(params  =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.email = this.router.getCurrentNavigation().extras.state.msg;
        this.medico = this.router.getCurrentNavigation().extras.state.medico;
      }
    });
  }

  ngOnInit() {
    console.log(this.email)
    console.log('citas pendientes')
    console.log(this.mediserv.getCitasPendientesMostrar(this.email));
    this.citas = this.mediserv.getCitasPendientesMostrar(this.email);
  }


  recetaralPaciente(citas){
    let params: NavigationExtras = {
      state:{
        msgg:citas,
      }
    }

    console.log('hkjhsdffdghdfsjgdhsfhsdg')
    console.log(params)
    this.router.navigate(['/recetar-al-paciente'],params);
  }
  
  RechazarCita(citas){
    let params: NavigationExtras = {
      state:{
        msggci:citas,
      }
    }

    console.log('rechazar-citaaa')
    console.log(params)
    this.router.navigate(['/rechazar-cita'],params);
  }

  EditarCuenta(){
    let params: NavigationExtras = {
      state:{
        msg:this.medico.uid,
      }
    }

    console.log('hkjhsdffdghdfsjgdhsfhsdg')
    console.log(params)
    this.router.navigate(['/cuenta-medicos'],params);
  }

  CitasConcluidas(citas){
    //this.citas = this.mediserv.getCitasPendientesMostrar(this.email);
    let params: NavigationExtras = {
      state:{
        mscitas:this.medico.uid,
      }
    }

    console.log(params)
    this.router.navigate(['/citas-medicas-asignadas'],params);
  }

  FacturaPaciente(citas){
    let params: NavigationExtras = {
      state:{
        msscitas:this.medico.uid,
      }
    }

    console.log(params)
    this.router.navigate(['/factura'],params);
  }
}