import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearDosPage } from './crear-dos.page';

const routes: Routes = [
  {
    path: '',
    component: CrearDosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearDosPageRoutingModule {}
