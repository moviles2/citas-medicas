import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-crear-dos',
  templateUrl: './crear-dos.page.html',
  styleUrls: ['./crear-dos.page.scss'],
})
export class CrearDosPage implements OnInit {

  uid:string   //identificador del documento
 
  worked: boolean = false;

  persona:Medicos = new Medicos; 

  constructor(public router: Router,public contactServ:MedicoService,private route:ActivatedRoute) {
    this.uid = this.route.snapshot.paramMap.get('uid');
    console.log("consultando ", this.uid);

    this.contactServ.getMedicoById2(this.uid).subscribe(data => {
        console.log(data)
        const aux:any = data
        this.persona = aux[0];
    });
   }

  ngOnInit() {
  }


  registrarEditarMedico(){
    this.contactServ.saveMedicoDos(this.persona);
    this.router.navigate(['/iniciarsesion-medicos']);
  }
}
