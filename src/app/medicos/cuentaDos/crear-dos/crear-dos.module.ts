import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearDosPageRoutingModule } from './crear-dos-routing.module';

import { CrearDosPage } from './crear-dos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearDosPageRoutingModule
  ],
  declarations: [CrearDosPage]
})
export class CrearDosPageModule {}
