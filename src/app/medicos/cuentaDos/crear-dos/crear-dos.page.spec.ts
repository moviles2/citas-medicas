import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearDosPage } from './crear-dos.page';

describe('CrearDosPage', () => {
  let component: CrearDosPage;
  let fixture: ComponentFixture<CrearDosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearDosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearDosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
