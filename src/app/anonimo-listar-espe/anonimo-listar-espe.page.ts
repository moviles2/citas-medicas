import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MedicoService } from '../services/medico.service';

@Component({
  selector: 'app-anonimo-listar-espe',
  templateUrl: './anonimo-listar-espe.page.html',
  styleUrls: ['./anonimo-listar-espe.page.scss'],
})
export class AnonimoListarEspePage implements OnInit {

  medicos: Observable<any[]>;
  
  constructor(private route:ActivatedRoute,private router:Router,public mediserv:MedicoService) {

   }

  ngOnInit() {
   this.medicos = this.mediserv.getMedicos();
  }

}
