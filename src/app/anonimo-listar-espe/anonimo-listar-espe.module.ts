import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnonimoListarEspePageRoutingModule } from './anonimo-listar-espe-routing.module';

import { AnonimoListarEspePage } from './anonimo-listar-espe.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnonimoListarEspePageRoutingModule
  ],
  declarations: [AnonimoListarEspePage]
})
export class AnonimoListarEspePageModule {}
