import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnonimoListarEspePage } from './anonimo-listar-espe.page';

describe('AnonimoListarEspePage', () => {
  let component: AnonimoListarEspePage;
  let fixture: ComponentFixture<AnonimoListarEspePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnonimoListarEspePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnonimoListarEspePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
