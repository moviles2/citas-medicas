import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnonimoListarEspePage } from './anonimo-listar-espe.page';

const routes: Routes = [
  {
    path: '',
    component: AnonimoListarEspePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnonimoListarEspePageRoutingModule {}
