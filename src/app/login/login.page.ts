import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: User = new User();
  constructor(private authS: AuthService) { }

  ngOnInit() {
  }

  async login(){
    await this.authS.onLogin(this.user);
    console.log(this.user);
  }

}
