import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, PopoverController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CitasMedicas } from 'src/app/model/citasMedicas';
import { Medicos } from 'src/app/model/medico';
import { Paciente } from 'src/app/model/paciente';
import { AuthService } from 'src/app/services/auth.service';
import { CitasMedicasService } from 'src/app/services/citas-medicas.service';
import { LocationService } from 'src/app/services/location.service';
import { MedicoService } from 'src/app/services/medico.service';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-detalle-medicos',
  templateUrl: './detalle-medicos.page.html',
  styleUrls: ['./detalle-medicos.page.scss'],
})
export class DetalleMedicosPage implements OnInit {
  medico: Medicos
  cita: CitasMedicas = new CitasMedicas
  citasMed: Observable<any[]>;
  medicosLis: Observable<any[]>;

  citas: Observable<any[]>;

  title = 'Consultorio del Doctor';
  lat = -2.8969536605642725;
  lng = -79.00415549603791


  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }




  paciente: any = {

    id: '',
    data: {} as Paciente
  };


  constructor(public router: Router, private citaMedica: CitasMedicasService,
    private route: ActivatedRoute, public alertController: AlertController, public medicoService: MedicoService,
    private popoverCtrl: PopoverController, private auth: AuthService,
    private locationService: LocationService, private pacienteService: PacienteService) {
    console.log("Pagina detalles medico")
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.router.getCurrentNavigation().extras.queryParams) {
        this.medico = this.router.getCurrentNavigation().extras.queryParams.medico
        console.log(this.medico);

      }
    });

  }
  async ngOnInit() {
    //direcciones
    console.log("Citaaaaaa  Medica");

    (await this.auth.getUser()).subscribe(dat => {
      this.auth.getAdmin(dat.uid).subscribe((datos) => {
        if (datos.payload.data() != null) {
          this.paciente.id = datos.payload.id;
          this.paciente.data = datos.payload.data();
          this.citasMed = this.citaMedica.getPacientes2(this.paciente.data.nombres);
          this.medicosLis = this.citaMedica.getMedicos(this.medico.nombre);
          this.citasMed.forEach(cita => {
            //Aquí insertas el código de comparación que necesites
            console.log("Lista")
            console.log(cita)

          });

          this.medicosLis.forEach(medico => {
            //Aquí insertas el código de comparación que necesites
            console.log("Lista medico")
            console.log(medico)

          });

        }
      });
    });



  }

  recervar(medico) {
    console.log("medico de parametro")
    console.log(medico)
    this.cita.medico = medico
    console.log("Cita medica")
    //Cita medica
    console.log(this.cita)
    //navegar en diferente paginas
    this.cita.estado = "pendiente"
    this.cita.estadoFactura = "pendiente"
    this.citaMedica.saveCitaMedica(this.cita, this.paciente);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        cita: this.cita,

      }
    };

    //dirigise a otra pagina y pasarle los parametros
    //this.router.navigate(['/pag-principal-usuario']);



  }

  setNewLocation(event) {
    if (event) {
      this.newLocation.latitude = event.lat;
      this.newLocation.longitude = event.lng;
      this.locationService.getAddressOfLocation(this.newLocation);
    }
  }
  hora: string
  async horarios(data) {
    try {


      console.log("horario")

      this.citas = this.citaMedica.getCitaMedica3(this.paciente.data.nombres, this.medico.nombre);
      console.log("Lista de todas las citas medicas")
      console.log(this.citas)
      this.hora = this.cita.hora
      console.log("dataaaaaaaaaaaaaaaaaa")
      console.log(data)
      if (this.cita.hora == "09:00") {
        console.log("ocupado")
        await this.presentAlert('Ocupado', 'Listo');

      }else if(this.cita.hora == "10:00"){
        console.log("ocupado")
        await this.presentAlert('Ocupado', 'Listo');
      }else if(this.cita.hora == "11:00"){
        console.log("ocupado")
        await this.presentAlert('Ocupado', 'Listo');
      }else if(this.cita.hora == "12:00"){
        console.log("ocupado")
        await this.presentAlert('Ocupado', 'Listo');
      }
      this.citas.forEach(element => {
        console.log(element)

      });
    } catch (error) {

    }

  }

  async presentAlert(mensaje: string, titulo: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titulo,
      subHeader: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

}
