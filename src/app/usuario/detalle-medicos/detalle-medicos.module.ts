import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleMedicosPageRoutingModule } from './detalle-medicos-routing.module';

import { DetalleMedicosPage } from './detalle-medicos.page';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleMedicosPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDMNQzWtX8FAgLoMFdrMs4KC55TnLoA6hk'
    }),
    AgmDirectionModule,
  ],
  declarations: [DetalleMedicosPage]
})
export class DetalleMedicosPageModule {}
