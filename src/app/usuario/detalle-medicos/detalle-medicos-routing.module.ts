import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleMedicosPage } from './detalle-medicos.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleMedicosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleMedicosPageRoutingModule {}
