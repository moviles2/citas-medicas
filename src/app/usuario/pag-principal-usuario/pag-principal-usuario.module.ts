import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagPrincipalUsuarioPageRoutingModule } from './pag-principal-usuario-routing.module';

import { PagPrincipalUsuarioPage } from './pag-principal-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagPrincipalUsuarioPageRoutingModule
  ],
  declarations: [PagPrincipalUsuarioPage]
})
export class PagPrincipalUsuarioPageModule {}
