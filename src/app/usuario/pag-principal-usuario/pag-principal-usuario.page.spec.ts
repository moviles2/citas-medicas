import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagPrincipalUsuarioPage } from './pag-principal-usuario.page';

describe('PagPrincipalUsuarioPage', () => {
  let component: PagPrincipalUsuarioPage;
  let fixture: ComponentFixture<PagPrincipalUsuarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagPrincipalUsuarioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagPrincipalUsuarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
