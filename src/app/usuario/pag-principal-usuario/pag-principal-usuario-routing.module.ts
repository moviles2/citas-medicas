import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagPrincipalUsuarioPage } from './pag-principal-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: PagPrincipalUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagPrincipalUsuarioPageRoutingModule {}
