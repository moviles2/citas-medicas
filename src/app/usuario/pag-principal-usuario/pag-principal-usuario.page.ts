import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Medicos } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';
import { MenuController, PopoverController } from '@ionic/angular';
import { Paciente } from 'src/app/model/paciente';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-pag-principal-usuario',
  templateUrl: './pag-principal-usuario.page.html',
  styleUrls: ['./pag-principal-usuario.page.scss'],
})
export class PagPrincipalUsuarioPage implements OnInit {
  medicos: Observable< any []>;
  slides=[
    {
      img:'assets/imagenes/cuatro.jpg'
    },
    {
      img:'assets/imagenes/uno.png'
    },
    {
      img:'assets/imagenes/dos.jpg'
    },
    {
      img:'assets/imagenes/tres.jpg'
      
    }
  ]
  paciente: any = {

    id: '',
    data: {} as Paciente
  };


  constructor(private menu: MenuController,
    public router:Router,
    public medicosService:MedicoService,
    private popoverCtrl: PopoverController, 
    private auth: AuthService) { }

  async ngOnInit() {
   
    this.medicos=this.medicosService.getMedicos();
    console.log(this.medicos)
    this.medicos.forEach(element => {
      console.log(element)
      
    });



    //Datos generales

    (await this.auth.getUser()).subscribe(dat => {
      this.auth.getAdmin(dat.uid).subscribe((datos) =>{
        if(datos.payload.data() !=null){
          this.paciente.id = datos.payload.id;
          this.paciente.data = datos.payload.data();
        }else{

          this.paciente.data = {} as Paciente;
        }
      });
    });


  }

  detalleContacto(medico:Medicos){
    console.log("detalle contacto")
    console.log(medico);
    let navigationExtras: NavigationExtras={
      queryParams:{
        medico:medico
      }
    };

   

    this.router.navigate(['/detalle-medicos'],navigationExtras);

  }
}
