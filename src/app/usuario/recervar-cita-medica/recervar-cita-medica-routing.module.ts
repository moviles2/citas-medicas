import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecervarCitaMedicaPage } from './recervar-cita-medica.page';

const routes: Routes = [
  {
    path: '',
    component: RecervarCitaMedicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecervarCitaMedicaPageRoutingModule {}
