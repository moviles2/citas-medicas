import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecervarCitaMedicaPageRoutingModule } from './recervar-cita-medica-routing.module';

import { RecervarCitaMedicaPage } from './recervar-cita-medica.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecervarCitaMedicaPageRoutingModule
  ],
  declarations: [RecervarCitaMedicaPage]
})
export class RecervarCitaMedicaPageModule {}
