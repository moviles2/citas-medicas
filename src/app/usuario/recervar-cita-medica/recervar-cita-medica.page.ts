import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/model/paciente';
import { AuthService } from 'src/app/services/auth.service';
import { CitasMedicasService } from 'src/app/services/citas-medicas.service';

@Component({
  selector: 'app-recervar-cita-medica',
  templateUrl: './recervar-cita-medica.page.html',
  styleUrls: ['./recervar-cita-medica.page.scss'],
})
export class RecervarCitaMedicaPage implements OnInit {


  citas: Observable< any []>;
  citasAprovadas: Observable< any []>;
  citasRechasadas:Observable< any []>;

  paciente: any = {

    id: '',
    data: {} as Paciente
  };

  constructor(private citasService:CitasMedicasService
    ,private auth: AuthService) { }

 async ngOnInit() {
  
   

   //Datos generales

   (await this.auth.getUser()).subscribe(dat => {
    this.auth.getAdmin(dat.uid).subscribe((datos) =>{
      if(datos.payload.data() !=null){
        this.paciente.id = datos.payload.id;
        this.paciente.data = datos.payload.data();
        this.citas=this.citasService.getCitaMedicaPendientes(this.paciente.data.nombres);
        console.log("Lista de todas las citas medicas")
        console.log(this.citas)
        this.citas.forEach(element => {
          console.log(element)
          
        });
        this.citasAprovadas=this.citasService.getCitaMedicaAprovadas(this.paciente.data.nombres);
        console.log("Lista de todas las citas medicas")
        console.log(this.citasAprovadas)
        this.citasAprovadas.forEach(element => {
          console.log(element)
          
        });
      
        this.citasRechasadas=this.citasService.getCitaMedicaRechasadas(this.paciente.data.nombres);
        console.log("Lista de todas las citas medica rechasadas s")
        console.log(this.citasRechasadas)
        this.citasRechasadas.forEach(element => {
          console.log(element)
          
        });
      
      }else{

        this.paciente.data = {} as Paciente;
      }
    });
  });


  }

}
