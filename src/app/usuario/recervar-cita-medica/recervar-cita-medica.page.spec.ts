import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecervarCitaMedicaPage } from './recervar-cita-medica.page';

describe('RecervarCitaMedicaPage', () => {
  let component: RecervarCitaMedicaPage;
  let fixture: ComponentFixture<RecervarCitaMedicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecervarCitaMedicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecervarCitaMedicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
