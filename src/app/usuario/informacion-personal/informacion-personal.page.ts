import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/model/paciente';
import { AuthService } from 'src/app/services/auth.service';
import { CitasMedicasService } from 'src/app/services/citas-medicas.service';

@Component({
  selector: 'app-informacion-personal',
  templateUrl: './informacion-personal.page.html',
  styleUrls: ['./informacion-personal.page.scss'],
})
export class InformacionPersonalPage implements OnInit {

  constructor(private auth: AuthService, private citasSecive:CitasMedicasService) { }
  paciente: any = {

    id: '',
    data: {} as Paciente
  };
  pac: Paciente = new Paciente
  async ngOnInit() {
    (await this.auth.getUser()).subscribe(dat => {
      this.auth.getAdmin(dat.uid).subscribe((datos) => {
        if (datos.payload.data() != null) {
          this.paciente.id = datos.payload.id;
          this.paciente.data = datos.payload.data();

        } else {

          this.paciente.data = {} as Paciente;
        }
      });
    });
  }
  registrar(){

    console.log(this.paciente.data)
  }
}


