import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/model/paciente';
import { Receta } from 'src/app/model/receta.class';
import { AuthService } from 'src/app/services/auth.service';
import { CitasMedicasService } from 'src/app/services/citas-medicas.service';

@Component({
  selector: 'app-ver-receta',
  templateUrl: './ver-receta.page.html',
  styleUrls: ['./ver-receta.page.scss'],
})
export class VerRecetaPage implements OnInit {
  recetaMedica: Observable< any []>;
  paciente: any = {

    id: '',
    data: {} as Paciente
  };

  constructor(private auth: AuthService,private citasService:CitasMedicasService) { }

async ngOnInit() {
  
   

   //Datos generales

   (await this.auth.getUser()).subscribe(dat => {
    this.auth.getAdmin(dat.uid).subscribe((datos) =>{
      if(datos.payload.data() !=null){
        this.paciente.id = datos.payload.id;
        this.paciente.data = datos.payload.data();
        this.recetaMedica=this.citasService.getReceta(this.paciente.data.nombres);
        console.log("Lista de recetas medicas")
        console.log(this.recetaMedica)
        this.recetaMedica.forEach(element => {
          console.log(element)
          
        });
      
      }else{

        this.paciente.data = {} as Paciente;
      }
    });
  });
}
}
