import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/model/paciente';
import { AuthService } from 'src/app/services/auth.service';
import { CitasMedicasService } from 'src/app/services/citas-medicas.service';

@Component({
  selector: 'app-factura-paciente',
  templateUrl: './factura-paciente.page.html',
  styleUrls: ['./factura-paciente.page.scss'],
})
export class FacturaPacientePage implements OnInit {

  facturaPaciente:  Observable< any []>;
  paciente: any = {

    id: '',
    data: {} as Paciente
  };


  constructor(private auth: AuthService, private citaMedicaService: CitasMedicasService) { }

  async ngOnInit() {
    (await this.auth.getUser()).subscribe(dat => {
      this.auth.getAdmin(dat.uid).subscribe((datos) =>{
        if(datos.payload.data() !=null){
          this.paciente.id = datos.payload.id;
          this.paciente.data = datos.payload.data();
          this.facturaPaciente=this.citaMedicaService.getFacturasAprovadas(this.paciente.data.nombres);

        }else{

          this.paciente.data = {} as Paciente;
        }
      });
    });

  }

}
