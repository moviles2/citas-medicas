import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacturaPacientePage } from './factura-paciente.page';

describe('FacturaPacientePage', () => {
  let component: FacturaPacientePage;
  let fixture: ComponentFixture<FacturaPacientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturaPacientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacturaPacientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
