import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacturaPacientePage } from './factura-paciente.page';

const routes: Routes = [
  {
    path: '',
    component: FacturaPacientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacturaPacientePageRoutingModule {}
