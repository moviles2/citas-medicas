import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacturaPacientePageRoutingModule } from './factura-paciente-routing.module';

import { FacturaPacientePage } from './factura-paciente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacturaPacientePageRoutingModule
  ],
  declarations: [FacturaPacientePage]
})
export class FacturaPacientePageModule {}
