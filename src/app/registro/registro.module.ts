import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroPageRoutingModule } from './registro-routing.module';

import { RegistroPage } from './registro.page';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDMNQzWtX8FAgLoMFdrMs4KC55TnLoA6hk'
    })
  ],
  declarations: [RegistroPage]
})
export class RegistroPageModule {}
