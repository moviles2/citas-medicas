import { Component, OnInit } from '@angular/core';
import { Paciente } from '../model/paciente';
import { PacienteService } from '../services/paciente.service';
import { Router } from '@angular/router';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  paciente: Paciente = new Paciente();

  //-2.8969536605642725
  //-79.00415549603791
  title = 'My first AGM project';
  lat = -2.8969536605642725;
  lng = -79.00415549603791


  newLocation = {
    latitude: '',
    longitude: '',
    address: ''
  }
  constructor(private pacienteSrv: PacienteService, private router: Router,private locationService:LocationService) { }

  ngOnInit() {
  }

  setNewLocation(event){
    if(event){
      this.newLocation.latitude = event.lat;
      this.newLocation.longitude = event.lng;
      this.locationService.getAddressOfLocation(this.newLocation);
    }
  }

  registrar(newLocation){
//s    this.paciente.direccion=this.newLocation

    console.log(this.pacienteSrv.onRegister(this.paciente,this.newLocation, this.paciente.uid));
    this.router.navigateByUrl('login');
  }

}
