import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-contact2',
  templateUrl: './contact2.page.html',
  styleUrls: ['./contact2.page.scss'],
})
export class Contact2Page implements OnInit {

  uid:string //identificador del documento
  message:Mensaje=new Mensaje();
  worked:boolean=false;
  constructor(public router: Router,
    public contactService:ContactosService,private route: ActivatedRoute) {
         this.uid=this.route.snapshot.paramMap.get('uid');
        //recuperar el obgeto de la base de datos
        this.contactService.getContactoById2(this.uid).subscribe(data =>
          {
            console.log(data)
            const aux: any = data
            this.message =aux[0];
          });
  }

     

  ngOnInit() {
  }

  guardar(){

    console.log(this.message)
    //navegar en diferente paginas
    this.contactService.saveContacto(this.message);
      let navigationExtras: NavigationExtras={
      queryParams:{
        message:this.message,
        worked:this.worked
      }
    };
   
  //dirigise a otra pagina y pasarle los parametros
  this.router.navigate(['/confirmacionmensaje'],navigationExtras);
   

  }
}
