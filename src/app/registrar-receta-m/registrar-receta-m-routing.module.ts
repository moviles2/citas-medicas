import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarRecetaMPage } from './registrar-receta-m.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarRecetaMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrarRecetaMPageRoutingModule {}
