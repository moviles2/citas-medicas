import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarRecetaMPageRoutingModule } from './registrar-receta-m-routing.module';

import { RegistrarRecetaMPage } from './registrar-receta-m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarRecetaMPageRoutingModule
  ],
  declarations: [RegistrarRecetaMPage]
})
export class RegistrarRecetaMPageModule {}
