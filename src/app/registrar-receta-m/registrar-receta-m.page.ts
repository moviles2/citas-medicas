import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { RecetaMedica } from '../model/recetaMedica';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-registrar-receta-m',
  templateUrl: './registrar-receta-m.page.html',
  styleUrls: ['./registrar-receta-m.page.scss'],
})
export class RegistrarRecetaMPage implements OnInit {
  recetaMed: RecetaMedica = new RecetaMedica();
  constructor(public router: Router,
    public contactService:ContactosService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }

  guardarRecetaMedica(){

    console.log(this.recetaMed)
    //navegar en diferente paginas
   this.contactService.saveRecetaMedicas (this.recetaMed);
  //dirigise a otra pagina y pasarle los parametros   
  let navigationExtras: NavigationExtras={
    queryParams:{
      recetMed:this.recetaMed
    }
  };
 
//dirigise a otra pagina y pasarle los parametros
//this.router.navigate(['/confirmacionmensaje'],navigationExtras);
 


  }


}
