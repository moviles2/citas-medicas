import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-localizacion',
  templateUrl: './localizacion.page.html',
  styleUrls: ['./localizacion.page.scss'],
})
export class LocalizacionPage implements OnInit {
  title= 'My first amg project';
  lat= 51.33;
  lng=7.88;
  constructor() { }

  ngOnInit() {
  }

}
