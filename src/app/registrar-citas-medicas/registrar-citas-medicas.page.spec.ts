import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistrarCitasMedicasPage } from './registrar-citas-medicas.page';

describe('RegistrarCitasMedicasPage', () => {
  let component: RegistrarCitasMedicasPage;
  let fixture: ComponentFixture<RegistrarCitasMedicasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarCitasMedicasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistrarCitasMedicasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
