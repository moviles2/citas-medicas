import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarCitasMedicasPage } from './registrar-citas-medicas.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarCitasMedicasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrarCitasMedicasPageRoutingModule {}
