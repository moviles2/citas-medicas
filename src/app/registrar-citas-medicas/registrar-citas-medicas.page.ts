import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { CitasMedicas } from '../model/citasMedicas';
import { CitasMedicasService } from '../services/citas-medicas.service';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-registrar-citas-medicas',
  templateUrl: './registrar-citas-medicas.page.html',
  styleUrls: ['./registrar-citas-medicas.page.scss'],
})
export class RegistrarCitasMedicasPage implements OnInit {

  citasMed:CitasMedicas = new CitasMedicas();

  constructor(public router: Router,
    public contactService:ContactosService,
    private route: ActivatedRoute) {


     }

  ngOnInit() {
  }

  guardarCitaMedica(){

    console.log(this.citasMed)
    //navegar en diferente paginas
   this.contactService.saveCitasMedicas(this.citasMed);
  //dirigise a otra pagina y pasarle los parametros   
  let navigationExtras: NavigationExtras={
    queryParams:{
      citasMed:this.citasMed
    }
  };
 
//dirigise a otra pagina y pasarle los parametros
//this.router.navigate(['/confirmacionmensaje'],navigationExtras);
 


  }

}
