import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarCitasMedicasPageRoutingModule } from './registrar-citas-medicas-routing.module';

import { RegistrarCitasMedicasPage } from './registrar-citas-medicas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarCitasMedicasPageRoutingModule
  ],
  declarations: [RegistrarCitasMedicasPage]
})
export class RegistrarCitasMedicasPageModule {}
