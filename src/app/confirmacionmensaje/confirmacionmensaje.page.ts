import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { CitasMedicas } from '../model/citasMedicas';
import { Mensaje } from '../model/mensaje';

@Component({
  selector: 'app-confirmacionmensaje',
  templateUrl: './confirmacionmensaje.page.html',
  styleUrls: ['./confirmacionmensaje.page.scss'],
})
export class ConfirmacionmensajePage implements OnInit {

  citasMed: CitasMedicas;
  worked:boolean;

  constructor(private route: ActivatedRoute, private router: Router) { 


    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.router.getCurrentNavigation().extras.queryParams) {
        this.citasMed = this.router.getCurrentNavigation().extras.queryParams.citasMed
        console.log(this.citasMed);
       
      }
    });


  }


  ngOnInit() {
  }

}
